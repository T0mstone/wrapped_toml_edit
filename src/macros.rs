/**
Example:
```
# use wrapped_toml_edit::table_struct;
table_struct! {
	struct Foo {
		foo: wrapped_toml_edit::validate::IsArray
	}
	use @{
		hkt as TestHKT,
		validate as IsTest,
		split as TestSplit
	}
}
```
*/
#[macro_export]
macro_rules! table_struct {
	($(#[doc = $($doc:tt)*])*
	$v:vis struct $name:ident {
		$($(#[doc = $($fdoc:tt)*])* $fname:ident $($flit:literal)? : $vali:ty),* $(,)?
	}
	$uv:vis use @{
		hkt as $hkt:ident,
		$(owned as $ow:ident,)?
		$(mut as $mut:ident,)?
		$(validate as $validate:ident,)?
		split as $split:ident$(,)?
	}) => {
		$(#[doc = $($doc)*])*
		#[$crate::__perfect_derive::perfect_derive(Debug, Clone, PartialEq)]
		#[repr(transparent)]
		$v struct $name <'a, K: $crate::handle::GenericHandleKind> ($crate::TableHandle<'a, K>);

		impl <'a, K: $crate::handle::GenericHandleKind> $name<'a, K> {
			/// Wrap the given table.
			#[inline]
			pub fn new(inner: $crate::TableHandle<'a, K>) -> Self {
				Self(inner)
			}

			/// Mutably borrow `self` as a handle.
			pub fn as_mut(&mut self) -> $name<$crate::handle::Mut> {
				$name(self.0.as_mut())
			}

			$(
				$(#[doc = $($fdoc)*])*
				pub fn $fname(&mut self) -> $crate::validate::wrappers::ValidatingTableEntry<$vali> {
					self.0.as_mut().validating_entry($crate::__table_struct!(@fkey $($flit)? $fname))
				}
			)*
		}

		impl<'a> $name<'a, $crate::handle::Mut> {
			pub fn split_mut(self) -> $crate::validate::wrappers::ValidateMutResult<'a, $split <'a, $crate::handle::Mut>> {
				let mut res = $split {
					$($fname: ::std::option::Option::None),*
				};
				for (k, v) in self.0.iter_mut() {
					$(if k.get() == $crate::__table_struct!(@fkey $($flit)? $fname) {
						res.$fname = Some(<$vali as $crate::validate::ValidateItem>::validate(v)?);
					} else)* {
						// TODO: error for invalid keys?
					}
				}
				Ok(res)
			}
		}

		$uv struct $hkt (::std::convert::Infallible);
		impl $crate::handle::GenericHandleHKT for $hkt {
			type Handle<'a, K: $crate::handle::GenericHandleKind> = $name<'a, K>;

			#[inline]
			fn cast_owned_lt<'b>(handle: Self::Handle<'_, $crate::handle::Owned>) -> Self::Handle<'b, $crate::handle::Owned> {
				$name($crate::TableHandleHKT::cast_owned_lt(handle.0))
			}

			#[inline]
			fn as_mut<'a, K: $crate::handle::GenericHandleKind>(handle: &'a mut Self::Handle<'_, K>) -> Self::Handle<'a, $crate::handle::Mut> {
				$name($crate::TableHandleHKT::as_mut(&mut handle.0))
			}

			#[inline]
			fn into_dynamic<K: $crate::handle::GenericHandleKind>(handle: Self::Handle<'_, K>) -> Self::Handle<'_, $crate::handle::Dynamic> {
				$name($crate::TableHandleHKT::into_dynamic(handle.0))
			}
		}

		$($uv type $ow<'a> = $name<'a, $crate::handle::Owned>;)?
		$($uv type $mut<'a> = $name<'a, $crate::handle::Mut>;)?

		$(
			$uv enum $validate {}

			impl $crate::validate::ValidateItem for $validate {
				type Output = $hkt;

				fn validate<K: $crate::handle::GenericHandleKind>(
					item: $crate::ItemHandle<K>,
				) -> Result<<Self::Output as $crate::handle::GenericHandleHKT>::Handle<'_, K>, $crate::validate::MalformedItem<K>> {
					$crate::validate::IsTable::validate(item).map($name)
				}

				#[inline]
				fn unvalidate<K: $crate::handle::GenericHandleKind>(
					item: <Self::Output as $crate::handle::GenericHandleHKT>::Handle<'_, K>
				) -> $crate::ItemHandle<K> {
					item.0.into()
				}
			}
		)?

		#[$crate::__perfect_derive::perfect_derive(Debug, Clone, PartialEq)]
		$uv struct $split <'a, K: $crate::handle::GenericHandleKind> {
			$(
				$(#[doc = $($fdoc)*])*
				pub $fname: Option<$crate::validate::wrappers::ValidOutputHandle<'a, $vali, K>>
			),*
		}
	};
}

/// Not public API.
#[macro_export]
#[doc(hidden)]
macro_rules! __table_struct {
	(@fkey $l:literal $_i:ident) => {{
		// ensure this is a _string_ literal
		let x: &'static str = $l;
		x
	}};
	(@fkey $i:ident) => {
		stringify!($i)
	};
}

// TODO: enum macro (how to do the split thing???)
