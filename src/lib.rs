#![doc = include_str!("../README.md")]
#![warn(missing_docs)]
#![allow(clippy::tabs_in_doc_comments)]

use std::collections::HashMap;

pub use ::toml_edit;
use handle::{GenericHandleHKT, GenericHandleToOwned};

/// Not public API.
#[doc(hidden)]
#[rustfmt::skip]
pub use ::perfect_derive as __perfect_derive;

mod macros;
pub mod handle;
pub mod entry;
pub mod validate;

use perfect_derive::perfect_derive;
use toml_edit::{
	Array, ArrayOfTables, Datetime, Formatted, InlineTable, Item, KeyMut, Table, TableLike, Value,
};

pub use self::hkt::*;
use crate::entry::*;
use crate::handle::{Dynamic, FromOwned, GenericHandleKind, Mut, Owned, map_handle};

#[allow(rustdoc::redundant_explicit_links /* reason: this seems to be a bug and not work otherwise */)]
/// Shorthand for a basic handle of a [`Formatted`](toml_edit::Formatted).
pub type FHandle<'a, T, K> = <K as GenericHandleKind>::Handle<'a, Formatted<T>>;

mod hkt {
	use std::convert::Infallible as Never;

	use toml_edit::Formatted;

	use crate::handle::{
		BasicHandleHKT, Dynamic, GenericHandleHKT, GenericHandleKind, GenericHandleToOwned, Owned,
	};

	/// The [HKT](crate::handle::GenericHandleHKT) for [`FHandle`](super::FHandle).
	pub type FHandleHKT<T> = BasicHandleHKT<Formatted<T>>;

	macro_rules! impl_handle_hkt {
		($T:ident => $H:ident { $($Var:ident => $GK:ident),* }) => {
			#[doc = concat!("The [HKT](crate::handle::GenericHandleHKT) for [`", stringify!($H), "`](super::", stringify!($H), ").")]
			pub struct $T(Never);

			impl GenericHandleHKT for $T {
				type Handle<'a, K: GenericHandleKind> = super::$H<'a, K>;

				fn cast_owned_lt<'b>(
					handle: Self::Handle<'_, Owned>,
				) -> Self::Handle<'b, Owned> {
					match handle {
						$(
							super::$H::$Var(x) => super::$H::$Var($GK::cast_owned_lt(x))
						),*
					}
				}

				fn as_mut<'a, K: GenericHandleKind>(handle: &'a mut Self::Handle<'_, K>) -> Self::Handle<'a, $crate::handle::Mut> {
					match handle {
						$(
							super::$H::$Var(x) => super::$H::$Var($GK::as_mut::<K>(x))
						),*
					}
				}

				fn into_dynamic<K: GenericHandleKind>(
					handle: Self::Handle<'_, K>,
				) -> Self::Handle<'_, Dynamic> {
					match handle {
						$(
							super::$H::$Var(x) => super::$H::$Var($GK::into_dynamic::<K>(x))
						),*
					}
				}
			}
			impl GenericHandleToOwned for $T {
				fn into_owned<'b, K: GenericHandleKind>(
					handle: Self::Handle<'_, K>,
				) -> Self::Handle<'b, Owned> {
					match handle {
						$(
							super::$H::$Var(x) => super::$H::$Var($GK::into_owned::<K>(x))
						),*
					}
				}
				fn to_owned<'b, K: GenericHandleKind>(
					handle: &Self::Handle<'_, K>
				) -> Self::Handle<'b, Owned> {
					match handle {
						$(
							super::$H::$Var(x) => super::$H::$Var($GK::to_owned::<K>(x))
						),*
					}
				}
			}
		};
	}

	impl_handle_hkt!(PrimitiveHandleHKT => PrimitiveHandle {
		String => BasicHandleHKT,
		Integer => BasicHandleHKT,
		Float => BasicHandleHKT,
		Boolean => BasicHandleHKT,
		Datetime => BasicHandleHKT
	});

	impl_handle_hkt!(TableHandleHKT => TableHandle {
		Table => BasicHandleHKT,
		InlineTable => BasicHandleHKT
	});

	impl_handle_hkt!(ArrayHandleHKT => ArrayHandle {
		OfValues => BasicHandleHKT,
		OfTables => BasicHandleHKT
	});

	impl_handle_hkt!(ItemHandleHKT => ItemHandle {
		Primitive => PrimitiveHandleHKT,
		Array => ArrayHandleHKT,
		Table => TableHandleHKT
	});
}

macro_rules! display_enum {
	($T:ident [$f:expr] { $($Var:ident),*$(,)? }) => {
		#[cfg(feature = "display")]
		impl<'a, K: GenericHandleKind> std::fmt::Display for $T<'a, K> {
			fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
				match self {
					$($T::$Var(x) => std::fmt::Display::fmt($f(x), f),)*
				}
			}
		}
	};
}

/// A handle to a primitive value.
#[perfect_derive(Debug, Clone)]
#[allow(missing_docs)]
pub enum PrimitiveHandle<'a, K: GenericHandleKind = Dynamic> {
	String(FHandle<'a, String, K>),
	Integer(FHandle<'a, i64, K>),
	Float(FHandle<'a, f64, K>),
	Boolean(FHandle<'a, bool, K>),
	Datetime(FHandle<'a, Datetime, K>),
}

display_enum!(PrimitiveHandle [K::as_ref] {
	String, Integer, Float, Boolean, Datetime
});

/// A handle to a table or inline table.
#[perfect_derive(Debug, Clone)]
#[allow(missing_docs)]
pub enum TableHandle<'a, K: GenericHandleKind = Dynamic> {
	Table(K::Handle<'a, Table>),
	InlineTable(K::Handle<'a, InlineTable>),
}

impl<K: GenericHandleKind + FromOwned> Default for TableHandle<'_, K> {
	fn default() -> Self {
		TableHandle::InlineTable(K::from_owned(InlineTable::default()))
	}
}

display_enum!(TableHandle [K::as_ref] {
	Table, InlineTable
});

/// A handle to an array or array of tables.
#[perfect_derive(Debug, Clone)]
#[allow(missing_docs)]
pub enum ArrayHandle<'a, K: GenericHandleKind = Dynamic> {
	OfValues(K::Handle<'a, Array>),
	OfTables(K::Handle<'a, ArrayOfTables>),
}

impl<K: GenericHandleKind + FromOwned> Default for ArrayHandle<'_, K> {
	fn default() -> Self {
		ArrayHandle::OfValues(K::from_owned(Array::default()))
	}
}

display_enum!(ArrayHandle [K::as_ref] {
	OfValues, OfTables
});

/// A handle to any kind of item.
#[perfect_derive(Debug, Clone)]
#[allow(missing_docs)]
pub enum ItemHandle<'a, K: GenericHandleKind = Dynamic> {
	Primitive(PrimitiveHandle<'a, K>),
	Array(ArrayHandle<'a, K>),
	Table(TableHandle<'a, K>),
}

display_enum!(ItemHandle [std::convert::identity] {
	Primitive, Array, Table
});

macro_rules! shorthand_types {
	($([$($gen:tt)*])? /* end */) => {};
	($([$($gen:tt)*])? $Base:ident => $Owned:ident, $Mut:ident $(, $($rest:tt)*)?) => {
		#[doc = concat!("Shorthand for [`", stringify!($Base), "`]`<'a,`[`Owned`]`>`.")]
		pub type $Owned<'a $(, $($gen)*)?> = $Base<'a, $($($gen)*,)? Owned>;
		#[doc = concat!("Shorthand for [`", stringify!($Base), "`]`<'a,`[`Mut`]`>`.")]
		pub type $Mut<'a $(, $($gen)*)?> = $Base<'a, $($($gen)*,)? Mut>;
		$crate::shorthand_types!($([$($gen)*])? $($($rest)*)?);
	};
}
pub(crate) use shorthand_types;

shorthand_types! {
	PrimitiveHandle => PrimitiveOwned, PrimitiveMut,
	TableHandle => TableOwned, TableMut,
	ArrayHandle => ArrayOwned, ArrayMut,
	ItemHandle => ItemOwned, ItemMut,
}

impl PrimitiveOwned<'_> {
	/// Extract the value that `self` is.
	pub fn into_value(self) -> Value {
		match self {
			PrimitiveHandle::String(x) => Value::String(x),
			PrimitiveHandle::Integer(x) => Value::Integer(x),
			PrimitiveHandle::Float(x) => Value::Float(x),
			PrimitiveHandle::Boolean(x) => Value::Boolean(x),
			PrimitiveHandle::Datetime(x) => Value::Datetime(x),
		}
	}
}

impl TableOwned<'_> {
	/// Extract the item that `self` is (table or inline table).
	pub fn into_item(self) -> Item {
		match self {
			TableHandle::Table(x) => Item::Table(x),
			TableHandle::InlineTable(x) => Item::Value(Value::InlineTable(x)),
		}
	}

	/// Like [`into_item`](Self::into_item), but convert an inline table to a (regular) table.
	pub fn into_table(self) -> Table {
		self.into_item().into_table().unwrap_or_else(|it| {
			unreachable!("`Item::into_table` returned `Err` on a table ({it:?})")
		})
	}
}

impl ArrayOwned<'_> {
	/// Extract the item that `self` is (array or array of tables).
	pub fn into_item(self) -> Item {
		match self {
			ArrayHandle::OfValues(x) => Item::Value(Value::Array(x)),
			ArrayHandle::OfTables(x) => Item::ArrayOfTables(x),
		}
	}
}

impl<K: GenericHandleKind> PrimitiveHandle<'_, K> {
	/// Borrow `self` mutably as a handle.
	#[inline]
	pub fn as_mut(&mut self) -> PrimitiveMut {
		<PrimitiveHandleHKT as GenericHandleHKT>::as_mut(self)
	}

	/// Convert `self` into an owned value, cloning if necessary.
	#[inline]
	pub fn into_owned<'o>(self) -> PrimitiveOwned<'o> {
		<PrimitiveHandleHKT as GenericHandleToOwned>::into_owned(self)
	}

	/// Convert `self` into an owned value by cloning.
	#[inline]
	pub fn to_owned<'o>(&self) -> PrimitiveOwned<'o> {
		<PrimitiveHandleHKT as GenericHandleToOwned>::to_owned(self)
	}
}

impl<'a, K: GenericHandleKind> ItemHandle<'a, K> {
	/// Construct an item handle from a basic handle of a [`Value`] (infallible).
	pub fn from_value(value: K::Handle<'a, Value>) -> Self {
		map_handle!([K; Value => <'a, K> ItemHandle<'a, K>]
			match value {
				Mut(value) => match value {
					Value::String(x) => ItemHandle::Primitive(PrimitiveHandle::String(K::from_mut(x))),
					Value::Integer(x) => ItemHandle::Primitive(PrimitiveHandle::Integer(K::from_mut(x))),
					Value::Float(x) => ItemHandle::Primitive(PrimitiveHandle::Float(K::from_mut(x))),
					Value::Boolean(x) => ItemHandle::Primitive(PrimitiveHandle::Boolean(K::from_mut(x))),
					Value::Datetime(x) => {
						ItemHandle::Primitive(PrimitiveHandle::Datetime(K::from_mut(x)))
					}
					Value::Array(x) => ItemHandle::Array(ArrayHandle::OfValues(K::from_mut(x))),
					Value::InlineTable(x) => ItemHandle::Table(TableHandle::InlineTable(K::from_mut(x))),
				},
				Owned(value) => match value {
					Value::String(x) => ItemHandle::Primitive(PrimitiveHandle::String(K::from_owned(x))),
					Value::Integer(x) => {
						ItemHandle::Primitive(PrimitiveHandle::Integer(K::from_owned(x)))
					}
					Value::Float(x) => ItemHandle::Primitive(PrimitiveHandle::Float(K::from_owned(x))),
					Value::Boolean(x) => {
						ItemHandle::Primitive(PrimitiveHandle::Boolean(K::from_owned(x)))
					}
					Value::Datetime(x) => {
						ItemHandle::Primitive(PrimitiveHandle::Datetime(K::from_owned(x)))
					}
					Value::Array(x) => ItemHandle::Array(ArrayHandle::OfValues(K::from_owned(x))),
					Value::InlineTable(x) => {
						ItemHandle::Table(TableHandle::InlineTable(K::from_owned(x)))
					}
				}
			}
		)
	}

	/// Construct an item handle from a basic handle of an [`Item`].
	/// Returns `None` when the input is [`Item::None`].
	pub fn new(item: K::Handle<'a, Item>) -> Option<Self> {
		map_handle!([K; Item => <'a, K> Option<ItemHandle<'a, K>>]
			match item {
				Mut(item) => Some(match item {
					Item::None => return None,
					Item::Value(x) => ItemHandle::<K>::from_value(K::from_mut(x)),
					Item::Table(x) => ItemHandle::Table(TableHandle::Table(K::from_mut(x))),
					Item::ArrayOfTables(x) => ItemHandle::Array(ArrayHandle::OfTables(K::from_mut(x))),
				}),
				Owned(item) => Some(match item {
					Item::None => return None,
					Item::Value(x) => ItemHandle::from_value(K::from_owned(x)),
					Item::Table(x) => ItemHandle::Table(TableHandle::Table(K::from_owned(x))),
					Item::ArrayOfTables(x) => ItemHandle::Array(ArrayHandle::OfTables(K::from_owned(x))),
				})
			}
		)
	}

	/// Borrow `self` mutably as a handle.
	#[inline]
	pub fn as_mut(&mut self) -> ItemMut {
		<ItemHandleHKT as GenericHandleHKT>::as_mut(self)
	}

	/// Convert `self` into an owned value, cloning if necessary.
	#[inline]
	pub fn into_owned<'o>(self) -> ItemOwned<'o> {
		<ItemHandleHKT as GenericHandleToOwned>::into_owned(self)
	}

	/// Convert `self` into an owned value by cloning.
	#[inline]
	pub fn to_owned<'o>(&self) -> ItemOwned<'o> {
		<ItemHandleHKT as GenericHandleToOwned>::to_owned(self)
	}

	/// Cast `self` to primitive.
	pub fn into_primitive(self) -> Result<PrimitiveHandle<'a, K>, Self> {
		match self {
			Self::Primitive(x) => Ok(x),
			item => Err(item),
		}
	}

	/// Cast `self` to table.
	pub fn into_table(self) -> Result<TableHandle<'a, K>, Self> {
		match self {
			Self::Table(x) => Ok(x),
			item => Err(item),
		}
	}

	/// Cast `self` to array.
	pub fn into_array(self) -> Result<ArrayHandle<'a, K>, Self> {
		match self {
			Self::Array(x) => Ok(x),
			item => Err(item),
		}
	}
}

impl ItemOwned<'_> {
	/// Extract the item that `self` is.
	pub fn into_item(self) -> Item {
		match self {
			ItemHandle::Primitive(x) => Item::Value(x.into_value()),
			ItemHandle::Array(x) => x.into_item(),
			ItemHandle::Table(x) => x.into_item(),
		}
	}

	/// Like [`into_item`](Self::into_item),
	/// but convert a table to an inline table and an array of tables to an array of inline tables.
	pub fn into_value(self) -> Value {
		self.into_item()
			.into_value()
			// note: this comes from `self`, which can't be Item::None.
			// The fact that `into_value` is `Ok` for all other values
			// is just based on its current implementation,
			// but I think it's a fair assumption to make, even for the future.
			.expect("Item::into_value errored for item other than Item::None")
	}
}

#[cfg(feature = "either")]
impl<'a, K: GenericHandleKind, A: Into<Self>, B: Into<Self>> From<either::Either<A, B>>
	for ItemHandle<'a, K>
{
	fn from(value: either::Either<A, B>) -> Self {
		value.map_either(A::into, B::into).into_inner()
	}
}

impl<'a, K: GenericHandleKind> From<PrimitiveHandle<'a, K>> for ItemHandle<'a, K> {
	fn from(value: PrimitiveHandle<'a, K>) -> Self {
		Self::Primitive(value)
	}
}

impl<'a, K: GenericHandleKind> From<ArrayHandle<'a, K>> for ItemHandle<'a, K> {
	fn from(value: ArrayHandle<'a, K>) -> Self {
		Self::Array(value)
	}
}

impl<'a, K: GenericHandleKind> From<TableHandle<'a, K>> for ItemHandle<'a, K> {
	fn from(value: TableHandle<'a, K>) -> Self {
		Self::Table(value)
	}
}

impl<K: GenericHandleKind> TableHandle<'_, K> {
	/// Borrow `self` mutably as a handle.
	#[inline]
	pub fn as_mut(&mut self) -> TableMut {
		<TableHandleHKT as GenericHandleHKT>::as_mut(self)
	}

	/// Convert `self` into an owned value, cloning if necessary.
	#[inline]
	pub fn into_owned<'o>(self) -> TableOwned<'o> {
		<TableHandleHKT as GenericHandleToOwned>::into_owned(self)
	}

	/// Convert `self` into an owned value by cloning.
	#[inline]
	pub fn to_owned<'o>(&self) -> TableOwned<'o> {
		<TableHandleHKT as GenericHandleToOwned>::to_owned(self)
	}

	/// Returns a [`TableLike`] trait object of `self`.
	pub fn as_dyn(&self) -> &dyn TableLike {
		match self {
			TableHandle::Table(tbl) => K::as_ref(tbl) as _,
			TableHandle::InlineTable(tbl) => K::as_ref(tbl) as _,
		}
	}
}

impl<'a> TableMut<'a> {
	/// Like [`Table::get_mut`].
	pub fn get_mut(self, key: &str) -> Option<ItemMut<'a>> {
		match self {
			Self::Table(tbl) => tbl.get_mut(key).and_then(ItemHandle::new),
			Self::InlineTable(tbl) => tbl.get_mut(key).map(ItemHandle::from_value),
		}
	}

	/// Like [`Table::entry`].
	pub fn entry(self, key: &str) -> TableEntry<'a> {
		match self {
			TableHandle::Table(tbl) => TableEntry::from(tbl.entry(key)),
			TableHandle::InlineTable(tbl) => TableEntry::from(tbl.entry(key)),
		}
	}

	/// Like [`Table::insert`], but fails if `self` already contains `key` (returning `Err(item)`).
	#[allow(clippy::result_large_err)]
	pub fn try_insert<'o>(self, key: &str, item: ItemOwned<'o>) -> Result<(), ItemOwned<'o>> {
		match self {
			TableHandle::Table(tbl) => {
				if tbl.contains_key(key) {
					return Err(item);
				}
				tbl.insert(key, item.into_item());
			}
			TableHandle::InlineTable(tbl) => {
				if tbl.contains_key(key) {
					return Err(item);
				}
				tbl.insert(key, item.into_value());
			}
		}
		Ok(())
	}

	/// Returns a [`TableLike`] trait object of `self`.
	pub fn as_mut_dyn(self) -> &'a mut dyn TableLike {
		match self {
			TableHandle::Table(tbl) => tbl as _,
			TableHandle::InlineTable(tbl) => tbl as _,
		}
	}

	/// Returns an iterator of mutable key-value pairs in `self`.
	pub fn iter_mut(self) -> impl Iterator<Item = (KeyMut<'a>, ItemMut<'a>)> {
		// Note: `ItemMut::new` only returns `None` for `Item::None`,
		// for which ignoring is the correct thing to do here.
		// (Also, it shouldn't ever even appear, but just to be safe)
		self.as_mut_dyn()
			.iter_mut()
			.filter_map(|(k, v)| Some((k, ItemMut::new(v)?)))
	}

	/// Returns a `HashMap` with all the same data as `self`.
	/// *Caution*: Inserting into the `HashMap` doesn't affect `self`.
	pub fn split_mut(self) -> HashMap<KeyMut<'a>, ItemMut<'a>> {
		self.iter_mut().collect()
	}
}

impl<K: GenericHandleKind> ArrayHandle<'_, K> {
	/// Borrow `self` mutably as a handle.
	#[inline]
	pub fn as_mut(&mut self) -> ArrayMut {
		<ArrayHandleHKT as GenericHandleHKT>::as_mut(self)
	}

	/// Convert `self` into an owned value, cloning if necessary.
	#[inline]
	pub fn into_owned<'o>(self) -> ArrayOwned<'o> {
		<ArrayHandleHKT as GenericHandleToOwned>::into_owned(self)
	}

	/// Convert `self` into an owned value by cloning.
	#[inline]
	pub fn to_owned<'o>(&self) -> ArrayOwned<'o> {
		<ArrayHandleHKT as GenericHandleToOwned>::to_owned(self)
	}
}

impl<'a> ArrayMut<'a> {
	/// Like [`Array::get_mut`].
	pub fn get_mut(self, index: usize) -> Option<ItemMut<'a>> {
		match self {
			Self::OfValues(arr) => arr.get_mut(index).map(ItemHandle::from_value),
			Self::OfTables(arr) => arr
				.get_mut(index)
				.map(TableHandle::Table)
				.map(ItemHandle::Table),
		}
	}

	/// Like [`Array::push`],
	/// but fails if `self` is an array of tables and `item` is not a table (returning `Err(item)`).
	#[allow(clippy::result_large_err)]
	pub fn try_push(self, item: ItemOwned<'_>) -> Result<(), ItemOwned<'_>> {
		match self {
			ArrayHandle::OfValues(arr) => {
				arr.push(item.into_value());
			}
			ArrayHandle::OfTables(arr) => match item {
				ItemHandle::Table(tbl) => arr.push(tbl.into_table()),
				item => return Err(item),
			},
		}
		Ok(())
	}

	/// Returns an iterator of mutable key-value pairs in `self`.
	pub fn iter_mut(self) -> Box<dyn Iterator<Item = ItemMut<'a>> + 'a> {
		match self {
			ArrayHandle::OfValues(arr) => Box::new(arr.iter_mut().map(ItemMut::from_value)) as _,
			ArrayHandle::OfTables(arr) => Box::new(
				arr.iter_mut()
					.map(|tbl| ItemMut::Table(TableMut::Table(tbl))),
			) as _,
		}
	}

	/// Returns a `Vec` with all the same data as `self`.
	/// *Caution*: Inserting into the `Vec` doesn't affect `self`.
	pub fn split_mut(self) -> Vec<ItemMut<'a>> {
		match self {
			ArrayHandle::OfValues(arr) => arr.iter_mut().map(ItemMut::from_value).collect(),
			ArrayHandle::OfTables(arr) => arr
				.iter_mut()
				.map(|tbl| ItemMut::Table(TableMut::Table(tbl)))
				.collect(),
		}
	}
}

/// Equality ignoring formatting
pub trait ValueEq<Rhs = Self> {
	#[allow(missing_docs)]
	fn value_eq(&self, other: &Rhs) -> bool;
}

macro_rules! formatted_value_eq {
	($($T:ty),*) => {
		$(
			impl ValueEq for Formatted<$T> {
				fn value_eq(&self, other: &Self) -> bool {
					self.value() == other.value()
				}
			}
		)*
	};
}
formatted_value_eq!(String, i64, f64, bool, toml_edit::Datetime);

macro_rules! value_eq {
	($e:expr, $T:ident; $($Var:ident),*) => {
		value_eq!($e, $T; $($Var),*; std::convert::identity, std::convert::identity)
	};
	($e:expr, $T:ident; $($Var:ident),*; $LF:expr, $RF:expr) => {
		match $e {
			$(
				($T::$Var(l), $T::$Var(r)) => $LF(l).value_eq($RF(r)),
			)*
			_ => false
		}
	};
}

impl ValueEq for Item {
	fn value_eq(&self, other: &Self) -> bool {
		if let (Item::None, Item::None) = (self, other) {
			return true;
		}
		value_eq!((self, other), Item; Value, Table, ArrayOfTables)
	}
}

impl ValueEq for Value {
	fn value_eq(&self, other: &Value) -> bool {
		value_eq!((self, other), Value; String, Integer, Float, Boolean, Datetime, Array, InlineTable)
	}
}

impl ValueEq for Array {
	fn value_eq(&self, other: &Self) -> bool {
		self.len() == other.len() && self.iter().zip(other.iter()).all(|(l, r)| l.value_eq(r))
	}
}

impl ValueEq for ArrayOfTables {
	fn value_eq(&self, other: &Self) -> bool {
		self.len() == other.len() && self.iter().zip(other.iter()).all(|(l, r)| l.value_eq(r))
	}
}

impl ValueEq for Table {
	fn value_eq(&self, other: &Self) -> bool {
		self.len() == other.len()
			&& self
				.iter()
				.all(|(k, l)| other.get(k).is_some_and(|r| l.value_eq(r)))
	}
}

impl ValueEq for InlineTable {
	fn value_eq(&self, other: &Self) -> bool {
		self.len() == other.len()
			&& self
				.iter()
				.all(|(k, l)| other.get(k).is_some_and(|r| l.value_eq(r)))
	}
}

impl<'b, K: GenericHandleKind, K2: GenericHandleKind> ValueEq<ItemHandle<'b, K2>>
	for ItemHandle<'_, K>
{
	fn value_eq(&self, other: &ItemHandle<'b, K2>) -> bool {
		value_eq!((self, other), ItemHandle; Primitive, Array, Table)
	}
}

impl<'b, K: GenericHandleKind, K2: GenericHandleKind> ValueEq<ArrayHandle<'b, K2>>
	for ArrayHandle<'_, K>
{
	fn value_eq(&self, other: &ArrayHandle<'b, K2>) -> bool {
		value_eq!((self, other), ArrayHandle; OfValues, OfTables; K::as_ref, K2::as_ref)
	}
}

impl<'b, K: GenericHandleKind, K2: GenericHandleKind> ValueEq<TableHandle<'b, K2>>
	for TableHandle<'_, K>
{
	fn value_eq(&self, other: &TableHandle<'b, K2>) -> bool {
		let this = self.as_dyn();
		let other = other.as_dyn();
		this.len() == other.len()
			&& this
				.iter()
				.all(|(k, l)| other.get(k).is_some_and(|r| l.value_eq(r)))
	}
}

impl<'b, K: GenericHandleKind, K2: GenericHandleKind> ValueEq<PrimitiveHandle<'b, K2>>
	for PrimitiveHandle<'_, K>
{
	fn value_eq(&self, other: &PrimitiveHandle<'b, K2>) -> bool {
		value_eq!((self, other), PrimitiveHandle; String, Integer, Float, Boolean, Datetime; K::as_ref, K2::as_ref)
	}
}
