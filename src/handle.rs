//! A mutable value that is either borrowed or owned.

use std::convert::Infallible as Never;
use std::marker::PhantomData;
use std::ops::{Deref, DerefMut};

pub use self::kind::*;

/// A mutable value that is either borrowed or owned.
///
/// This is similar to [`Cow<'a, T>`](std::borrow::Cow),
/// but doesn't require [`ToOwned`]
/// and has a mutable reference instead of a shared one.
#[derive(Debug)]
#[allow(missing_docs)]
pub enum DynamicHandle<'a, T> {
	Mut(&'a mut T),
	Owned(T),
}

impl<'a, T> From<&'a mut T> for DynamicHandle<'a, T> {
	fn from(value: &'a mut T) -> Self {
		Self::Mut(value)
	}
}

impl<T> From<T> for DynamicHandle<'_, T> {
	fn from(value: T) -> Self {
		Self::Owned(value)
	}
}

impl<T> DynamicHandle<'_, T> {
	/// Like [`Cow::into_owned`](std::borrow::Cow::into_owned):
	/// Converts `self` into an owned value by cloning if necessary.
	#[inline]
	pub fn into_owned(self) -> T
	where
		T: Clone,
	{
		match self {
			Self::Mut(inner) => inner.clone(),
			Self::Owned(inner) => inner,
		}
	}
}

impl<T> Deref for DynamicHandle<'_, T> {
	type Target = T;

	#[inline]
	fn deref(&self) -> &Self::Target {
		match self {
			Self::Mut(x) => x,
			Self::Owned(x) => x,
		}
	}
}

impl<T> DerefMut for DynamicHandle<'_, T> {
	#[inline]
	fn deref_mut(&mut self) -> &mut Self::Target {
		match self {
			Self::Mut(x) => x,
			Self::Owned(x) => x,
		}
	}
}

impl<T> AsRef<T> for DynamicHandle<'_, T> {
	#[inline]
	fn as_ref(&self) -> &T {
		self
	}
}

impl<T> AsMut<T> for DynamicHandle<'_, T> {
	#[inline]
	fn as_mut(&mut self) -> &mut T {
		self
	}
}

mod kind {
	use super::*;

	/// The kind of a generic handle: [`Owned`], [`Mut`] or [`Dynamic`].
	pub trait GenericHandleKind: 'static + Sized {
		#[allow(missing_docs)]
		type Handle<'a, T: 'a>: Into<DynamicHandle<'a, T>>;

		/// Borrow the inner data.
		fn as_ref<'a, 'b, T: 'a>(handle: &'b Self::Handle<'a, T>) -> &'b T;
		/// Borrow the inner data mutably.
		fn as_mut<'a, 'b, T: 'a>(handle: &'b mut Self::Handle<'a, T>) -> &'b mut T;

		/// Apply a generic function to the inner data.
		fn map<'a, T: 'a, F: GenericHandleFnOnce<T>>(
			handle: Self::Handle<'a, T>,
			f: F,
		) -> F::Output<'a, Self>;
	}

	/// A kind of handle that can be owned.
	#[allow(missing_docs)]
	pub trait FromOwned: GenericHandleKind {
		fn from_owned<'a, T: 'a>(t: T) -> Self::Handle<'a, T>;
	}
	/// A kind of handle that can be borrowed.
	#[allow(missing_docs)]
	pub trait FromMut: GenericHandleKind {
		fn from_mut<'a, T: 'a>(t: &'a mut T) -> Self::Handle<'a, T>;
	}

	/// A function generic over the kind of a handle.
	///
	/// Implementors should make sure that `call_with_mut` and `call_with_owned`
	/// semantically do the same thing.
	#[allow(missing_docs)]
	pub trait GenericHandleFnOnce<T> {
		type Output<'a, K: GenericHandleKind>;

		fn call_with_mut<K: GenericHandleKind + FromMut>(
			self,
			value: &mut T,
		) -> Self::Output<'_, K>;
		fn call_with_owned<'a, K: GenericHandleKind + FromOwned>(
			self,
			value: T,
		) -> Self::Output<'a, K>;
	}

	macro_rules! map_handle {
		([$K:ty; $T:ty => <$a:lifetime, $GK:ident> $U:ty]
			match $handle:tt {
				Mut($m:ident) => $me:expr,
				Owned($o:ident) => $oe:expr
			}
		) => {{
			struct AdHoc;

			impl $crate::handle::GenericHandleFnOnce<$T> for AdHoc {
				type Output<$a, $GK: $crate::handle::GenericHandleKind> = $U;

				fn call_with_mut<
					$GK: $crate::handle::GenericHandleKind + $crate::handle::FromMut,
				>(
					self,
					$m: &mut $T,
				) -> Self::Output<'_, $GK> {
					$me
				}
				fn call_with_owned<
					'a,
					$GK: $crate::handle::GenericHandleKind + $crate::handle::FromOwned,
				>(
					self,
					$o: $T,
				) -> Self::Output<'a, $GK> {
					$oe
				}
			}

			<$K as $crate::handle::GenericHandleKind>::map($handle, AdHoc)
		}};
	}
	pub(crate) use map_handle;

	/// The [`GenericHandleKind`] of an always-owned handle.
	pub struct Owned(Never);
	/// The [`GenericHandleKind`] of an always-borrowed handle.
	pub struct Mut(Never);
	/// The [`GenericHandleKind`] of a [`DynamicHandle`].
	pub struct Dynamic(Never);

	impl GenericHandleKind for Dynamic {
		type Handle<'a, T: 'a> = DynamicHandle<'a, T>;

		#[inline]
		fn as_ref<'a, 'b, T: 'a>(handle: &'b Self::Handle<'a, T>) -> &'b T {
			handle
		}
		#[inline]
		fn as_mut<'a, 'b, T: 'a>(handle: &'b mut Self::Handle<'a, T>) -> &'b mut T {
			handle
		}

		#[inline]
		fn map<'a, T: 'a, F: GenericHandleFnOnce<T>>(
			handle: Self::Handle<'a, T>,
			f: F,
		) -> F::Output<'a, Self> {
			match handle {
				DynamicHandle::Mut(x) => f.call_with_mut::<Self>(x),
				DynamicHandle::Owned(x) => f.call_with_owned::<Self>(x),
			}
		}
	}
	impl GenericHandleKind for Mut {
		type Handle<'a, T: 'a> = &'a mut T;

		#[inline]
		fn as_ref<'a, 'b, T: 'a>(handle: &'b Self::Handle<'a, T>) -> &'b T {
			handle
		}
		#[inline]
		fn as_mut<'a, 'b, T: 'a>(handle: &'b mut Self::Handle<'a, T>) -> &'b mut T {
			handle
		}

		#[inline]
		fn map<'a, T: 'a, F: GenericHandleFnOnce<T>>(
			handle: Self::Handle<'a, T>,
			f: F,
		) -> F::Output<'a, Self> {
			f.call_with_mut::<Self>(handle)
		}
	}
	impl GenericHandleKind for Owned {
		type Handle<'a, T: 'a> = T;

		#[inline]
		fn as_ref<'a, 'b, T: 'a>(handle: &'b Self::Handle<'a, T>) -> &'b T {
			handle
		}
		#[inline]
		fn as_mut<'a, 'b, T: 'a>(handle: &'b mut Self::Handle<'a, T>) -> &'b mut T {
			handle
		}

		#[inline]
		fn map<'a, T: 'a, F: GenericHandleFnOnce<T>>(
			handle: Self::Handle<'a, T>,
			f: F,
		) -> F::Output<'a, Self> {
			f.call_with_owned::<Self>(handle)
		}
	}

	impl FromMut for Dynamic {
		#[inline]
		fn from_mut<'a, T: 'a>(t: &'a mut T) -> Self::Handle<'a, T> {
			DynamicHandle::Mut(t)
		}
	}
	impl FromOwned for Dynamic {
		#[inline]
		fn from_owned<'a, T: 'a>(t: T) -> Self::Handle<'a, T> {
			DynamicHandle::Owned(t)
		}
	}
	impl FromMut for Mut {
		#[inline]
		fn from_mut<'a, T: 'a>(t: &'a mut T) -> Self::Handle<'a, T> {
			t
		}
	}
	impl FromOwned for Owned {
		#[inline]
		fn from_owned<'a, T: 'a>(t: T) -> Self::Handle<'a, T> {
			t
		}
	}
}

/// A stand-in for `Self::Handle`, where the generic parameters still need to be given.
///
/// Typically, `Self::Handle` will either be a [basic handle](BasicHandleHKT) or a struct or enum containing other handles.
pub trait GenericHandleHKT {
	/// The concrete handle for the given lifetime and kind.
	type Handle<'a, K: GenericHandleKind>;

	/// The [`Owned`] handle isn't allowed to depend on `'a`,
	/// which is proven by being able to implement this function.
	fn cast_owned_lt<'b>(handle: Self::Handle<'_, Owned>) -> Self::Handle<'b, Owned>;

	/// (Re-)Borrow a handle mutably.
	fn as_mut<'a, K: GenericHandleKind>(
		handle: &'a mut Self::Handle<'_, K>,
	) -> Self::Handle<'a, Mut>;

	/// Both [`Owned`] and [`Mut`] handles are semantically subtypes of the [`Dynamic`] handle.
	/// This is the corresponding upcasting function.
	fn into_dynamic<K: GenericHandleKind>(handle: Self::Handle<'_, K>)
	-> Self::Handle<'_, Dynamic>;
}

/// A handle that can be converted into its owned equivalent.
/// Similar to [`Cow::into_owned`](std::borrow::Cow::into_owned).
#[allow(missing_docs)]
pub trait GenericHandleToOwned: GenericHandleHKT {
	fn into_owned<'b, K: GenericHandleKind>(handle: Self::Handle<'_, K>)
	-> Self::Handle<'b, Owned>;

	fn to_owned<'b, K: GenericHandleKind>(handle: &Self::Handle<'_, K>) -> Self::Handle<'b, Owned>;
}

/// The basic generic handle for a type `T`.
/// Its concrete handle type is `T`, `&'a mut T`, or `DynamicHandle<'a, T>`, depending on the handle kind.
pub struct BasicHandleHKT<T>(PhantomData<T>, Never);

impl<T: 'static> GenericHandleHKT for BasicHandleHKT<T> {
	type Handle<'a, K: GenericHandleKind> = K::Handle<'a, T>;

	#[inline]
	fn cast_owned_lt<'b>(handle: Self::Handle<'_, Owned>) -> Self::Handle<'b, Owned> {
		handle
	}

	#[inline]
	fn as_mut<'a, K: GenericHandleKind>(handle: &'a mut Self::Handle<'_, K>) -> &'a mut T {
		K::as_mut(handle)
	}

	#[inline]
	fn into_dynamic<K: GenericHandleKind>(
		handle: Self::Handle<'_, K>,
	) -> Self::Handle<'_, Dynamic> {
		handle.into()
	}
}

impl<T: 'static + Clone> GenericHandleToOwned for BasicHandleHKT<T> {
	#[inline]
	fn into_owned<'b, K: GenericHandleKind>(
		handle: Self::Handle<'_, K>,
	) -> Self::Handle<'b, Owned> {
		Self::into_dynamic::<K>(handle).into_owned()
	}
	#[inline]
	fn to_owned<'b, K: GenericHandleKind>(handle: &Self::Handle<'_, K>) -> Self::Handle<'b, Owned> {
		K::as_ref(handle).clone()
	}
}

#[cfg(feature = "either")]
impl<A: GenericHandleHKT, B: GenericHandleHKT> GenericHandleHKT for either::Either<A, B> {
	type Handle<'a, K: GenericHandleKind> = either::Either<A::Handle<'a, K>, B::Handle<'a, K>>;

	fn cast_owned_lt<'b>(handle: Self::Handle<'_, Owned>) -> Self::Handle<'b, Owned> {
		handle.map_either(A::cast_owned_lt, B::cast_owned_lt)
	}

	fn as_mut<'a, K: GenericHandleKind>(
		handle: &'a mut Self::Handle<'_, K>,
	) -> Self::Handle<'a, Mut> {
		handle.as_mut().map_either(A::as_mut, B::as_mut)
	}

	fn into_dynamic<K: GenericHandleKind>(
		handle: Self::Handle<'_, K>,
	) -> Self::Handle<'_, Dynamic> {
		handle.map_either(A::into_dynamic, B::into_dynamic)
	}
}

#[cfg(feature = "either")]
impl<A: GenericHandleToOwned, B: GenericHandleToOwned> GenericHandleToOwned
	for either::Either<A, B>
{
	fn into_owned<'b, K: GenericHandleKind>(
		handle: Self::Handle<'_, K>,
	) -> Self::Handle<'b, Owned> {
		handle.map_either(A::into_owned, B::into_owned)
	}

	fn to_owned<'b, K: GenericHandleKind>(handle: &Self::Handle<'_, K>) -> Self::Handle<'b, Owned> {
		handle.as_ref().map_either(A::to_owned, B::to_owned)
	}
}

impl<A: GenericHandleHKT, B: GenericHandleHKT> GenericHandleHKT for (A, B) {
	type Handle<'a, K: GenericHandleKind> = (A::Handle<'a, K>, B::Handle<'a, K>);

	fn cast_owned_lt<'b>((a, b): Self::Handle<'_, Owned>) -> Self::Handle<'b, Owned> {
		(A::cast_owned_lt(a), B::cast_owned_lt(b))
	}

	fn as_mut<'a, K: GenericHandleKind>(
		(a, b): &'a mut Self::Handle<'_, K>,
	) -> Self::Handle<'a, Mut> {
		(A::as_mut(a), B::as_mut(b))
	}

	fn into_dynamic<K: GenericHandleKind>(
		(a, b): Self::Handle<'_, K>,
	) -> Self::Handle<'_, Dynamic> {
		(A::into_dynamic(a), B::into_dynamic(b))
	}
}

impl<A: GenericHandleToOwned, B: GenericHandleToOwned> GenericHandleToOwned for (A, B) {
	fn into_owned<'b, K: GenericHandleKind>(
		(a, b): Self::Handle<'_, K>,
	) -> Self::Handle<'b, Owned> {
		(A::into_owned(a), B::into_owned(b))
	}
	fn to_owned<'b, K: GenericHandleKind>((a, b): &Self::Handle<'_, K>) -> Self::Handle<'b, Owned> {
		(A::to_owned(a), B::to_owned(b))
	}
}
