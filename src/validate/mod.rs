//! Predicates that decide whether an item or value is "valid".

use std::convert::Infallible as Never;
use std::marker::PhantomData;

use perfect_derive::perfect_derive;

use crate::handle::{Dynamic, GenericHandleHKT, GenericHandleKind};
use crate::{
	ArrayHandleHKT, FHandleHKT, ItemHandle, ItemHandleHKT, PrimitiveHandle, TableHandleHKT,
};

pub mod wrappers;

/// An item that doesn't have the required type.
#[perfect_derive(Debug)]
pub struct MalformedItem<'a, K: GenericHandleKind = Dynamic>(pub ItemHandle<'a, K>);

/// A predicate that decides whether an [`Item`](ItemHandle) is valid.
pub trait ValidateItem {
	/// A handle to a type encoding a valid item for this predicate.
	type Output: GenericHandleHKT;

	/// Validate an item.
	fn validate<K: GenericHandleKind>(
		item: ItemHandle<K>,
	) -> Result<<Self::Output as GenericHandleHKT>::Handle<'_, K>, MalformedItem<K>>;

	/// Convert a validated value back into the corresponding item.
	///
	/// Roundtrips must result in identical values:
	/// - `validate(unvalidate(valid)) == Ok(valid)`
	/// - If `validate(item)` is `Ok(valid)`, then `unvalidate(valid) == item`.
	fn unvalidate<K: GenericHandleKind>(
		item: <Self::Output as GenericHandleHKT>::Handle<'_, K>,
	) -> ItemHandle<K>;
}

#[cfg(feature = "either")]
impl<A: ValidateItem, B: ValidateItem> ValidateItem for either::Either<A, B> {
	type Output = either::Either<A::Output, B::Output>;

	fn validate<K: GenericHandleKind>(
		item: ItemHandle<K>,
	) -> Result<<Self::Output as GenericHandleHKT>::Handle<'_, K>, MalformedItem<K>> {
		A::validate(item)
			.map(either::Either::Left)
			.or_else(|MalformedItem(item)| B::validate(item).map(either::Either::Right))
	}

	fn unvalidate<K: GenericHandleKind>(
		item: <Self::Output as GenericHandleHKT>::Handle<'_, K>,
	) -> ItemHandle<K> {
		item.map_either(A::unvalidate, B::unvalidate).into_inner()
	}
}

/// The trivial predicate: anything is valid.
pub struct IsAny(Never);
/// Only the [primitive type](PrimitiveHandle) `T` is valid.
pub struct IsPrimitive<T>(PhantomData<T>, Never);
/// Only [tables and inline tables](crate::TableHandle) are valid.
pub enum IsTable {}
/// Only [arrays and arrays of tables](crate::ArrayHandle) are valid.
pub enum IsArray {}

impl ValidateItem for IsAny {
	type Output = ItemHandleHKT;

	#[inline]
	fn validate<K: GenericHandleKind>(
		item: ItemHandle<K>,
	) -> Result<<Self::Output as GenericHandleHKT>::Handle<'_, K>, MalformedItem<K>> {
		Ok(item)
	}

	#[inline]
	fn unvalidate<K: GenericHandleKind>(
		item: <Self::Output as GenericHandleHKT>::Handle<'_, K>,
	) -> ItemHandle<K> {
		item
	}
}

macro_rules! validate_prim {
    ($($Var:ident => $t:ty),*) => {
        $(impl ValidateItem for IsPrimitive<$t> {
			type Output = FHandleHKT<$t>;

			fn validate<K: GenericHandleKind>(
				item: ItemHandle<K>,
			) -> Result<<Self::Output as GenericHandleHKT>::Handle<'_, K>, MalformedItem<K>> {
				item.into_primitive()
					.and_then(|pr| match pr {
						PrimitiveHandle::$Var(x) => Ok(x),
						pr => Err(ItemHandle::Primitive(pr)),
					})
					.map_err(MalformedItem)
			}

			fn unvalidate<K: GenericHandleKind>(
				item: <Self::Output as GenericHandleHKT>::Handle<'_, K>
			) -> ItemHandle<K> {
				ItemHandle::Primitive(PrimitiveHandle::$Var(item))
			}
		})*
    };
}
validate_prim!(String => String, Integer => i64, Float => f64, Boolean => bool, Datetime => toml_edit::Datetime);

impl ValidateItem for IsTable {
	type Output = TableHandleHKT;

	fn validate<K: GenericHandleKind>(
		item: ItemHandle<K>,
	) -> Result<<Self::Output as GenericHandleHKT>::Handle<'_, K>, MalformedItem<K>> {
		item.into_table().map_err(MalformedItem)
	}

	#[inline]
	fn unvalidate<K: GenericHandleKind>(
		item: <Self::Output as GenericHandleHKT>::Handle<'_, K>,
	) -> ItemHandle<K> {
		item.into()
	}
}

impl ValidateItem for IsArray {
	type Output = ArrayHandleHKT;

	fn validate<K: GenericHandleKind>(
		item: ItemHandle<K>,
	) -> Result<<Self::Output as GenericHandleHKT>::Handle<'_, K>, MalformedItem<K>> {
		item.into_array().map_err(MalformedItem)
	}

	#[inline]
	fn unvalidate<K: GenericHandleKind>(
		item: <Self::Output as GenericHandleHKT>::Handle<'_, K>,
	) -> ItemHandle<K> {
		item.into()
	}
}
