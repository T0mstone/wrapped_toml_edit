//! Wrappers that automatically (but lazily) validate their contents when accessed.

use std::convert::Infallible as Never;
use std::marker::PhantomData;

use perfect_derive::perfect_derive;
use toml_edit::{DocumentMut, KeyMut};

pub use self::hkt::*;
use super::{IsArray, IsTable, MalformedItem, ValidateItem};
use crate::handle::{
	Dynamic, FromOwned, GenericHandleHKT, GenericHandleKind, GenericHandleToOwned, Mut, Owned,
};
use crate::{
	ArrayHandle, ArrayMut, ItemHandle, ItemOwned, OccupiedTableEntry, TableEntry, TableHandle,
	TableMut, VacantTableEntry,
};

/// Similar to [`IsTable`], but wraps the output in [`ValidatingTableHandle`].
pub struct IsTableValidate<V: ValidateItem>(PhantomData<V>, Never);
/// Similar to [`IsArray`], but wraps the output in [`ValidatingArrayHandle`].
pub struct IsArrayValidate<V: ValidateItem>(PhantomData<V>, Never);

impl<V: ValidateItem> ValidateItem for IsTableValidate<V> {
	type Output = ValidatingTableHandleHKT<V>;

	fn validate<K: GenericHandleKind>(
		item: ItemHandle<K>,
	) -> Result<<Self::Output as GenericHandleHKT>::Handle<'_, K>, MalformedItem<K>> {
		IsTable::validate(item).map(ValidatingTableHandle::new)
	}

	#[inline]
	fn unvalidate<K: GenericHandleKind>(
		item: <Self::Output as GenericHandleHKT>::Handle<'_, K>,
	) -> ItemHandle<K> {
		item.inner.into()
	}
}

impl<V: ValidateItem> ValidateItem for IsArrayValidate<V> {
	type Output = ValidatingArrayHandleHKT<V>;

	fn validate<K: GenericHandleKind>(
		item: ItemHandle<K>,
	) -> Result<<Self::Output as GenericHandleHKT>::Handle<'_, K>, MalformedItem<K>> {
		IsArray::validate(item).map(ValidatingArrayHandle::new)
	}

	#[inline]
	fn unvalidate<K: GenericHandleKind>(
		item: <Self::Output as GenericHandleHKT>::Handle<'_, K>,
	) -> ItemHandle<K> {
		item.inner.into()
	}
}

/// Generic shorthand type for the output of the validation of an item handle.
pub type ValidOutputHandle<'a, V, K> =
	<<V as ValidateItem>::Output as GenericHandleHKT>::Handle<'a, K>;
/// Generic shorthand type for the output of the validation of a mutable item handle.
pub type ValidOutputMut<'a, V> = ValidOutputHandle<'a, V, Mut>;
/// Generic shorthand type for the output of the validation of an owned item handle.
pub type ValidOutputOwned<'a, V> = ValidOutputHandle<'a, V, Owned>;

/// Shorthand for the result of the validation of a mutable handle.
pub type ValidateMutResult<'a, T> = Result<T, MalformedItem<'a, Mut>>;
/// Shorthand for the common combination of [`ValidateMutResult`] and [`Option`].
pub type PropResult<'a, T> = ValidateMutResult<'a, Option<T>>;

mod hkt {
	use std::convert::Infallible as Never;
	use std::marker::PhantomData;

	use crate::handle::{
		Dynamic, GenericHandleHKT, GenericHandleKind, GenericHandleToOwned, Owned,
	};
	use crate::validate::ValidateItem;
	use crate::{ArrayHandleHKT, TableHandleHKT};

	/// The [HKT](GenericHandleHKT) for a [`ValidatingTableHandle`](super::ValidatingTableHandle).
	pub struct ValidatingTableHandleHKT<V: ValidateItem>(PhantomData<V>, Never);
	/// The [HKT](GenericHandleHKT) for a [`ValidatingArrayHandle`](super::ValidatingArrayHandle).
	pub struct ValidatingArrayHandleHKT<V: ValidateItem>(PhantomData<V>, Never);

	macro_rules! wrapper {
		([$T:ident => $W:ident] [|$h:ident, $wf:ident| $e:expr] $(fn $f:ident [$($gen:tt)*] ($A:ty) -> $B:ty;)*) => {
			$(#[inline]
			fn $f <$($gen)*> ($h: $A) -> $B {
				let $wf = $W::$f;
				$e
			})*
		};
		(impl$([$($igen:tt)*])? ($T:ident => $H:ident) $([$($tgen:tt)*])? => $W:ident
			|$h:ident, $wf:ident| $e:expr
		) => {
			impl$(<$($igen)*>)? GenericHandleHKT for $T$(<$($tgen)*>)? {
				// bit of a hack to hardcode the generic parameters like this, but it works (for now)
				type Handle<'a, K: GenericHandleKind> = super::$H<'a, $($($tgen)*,)? K>;

				wrapper!([$T => $W] [|$h, $wf| $e]
					fn cast_owned_lt ['a, 'b](Self::Handle<'a, Owned>) -> Self::Handle<'b, Owned>;
					fn into_dynamic ['a, K: GenericHandleKind](Self::Handle<'a, K>) -> Self::Handle<'a, Dynamic>;
				);

				#[inline]
				fn as_mut<'a, K: GenericHandleKind>(handle: &'a mut Self::Handle<'_, K>) -> Self::Handle<'a, $crate::handle::Mut> {
					// this makes `$h.inner` work in `$e`, abusing the fact that `$e` doesn't know what type `$h` is.
					struct Hack<'b, T> { inner: &'b mut T }
					let $h = Hack { inner: &mut handle.inner };
					let $wf = $W::as_mut;
					$e
				}
			}
			impl$(<$($igen)*>)? GenericHandleToOwned for $T$(<$($tgen)*>)? {
				wrapper!([$T => $W] [|$h, $wf| $e]
					fn into_owned ['o, K: GenericHandleKind](Self::Handle<'_, K>) -> Self::Handle<'o, Owned>;
				);
				#[inline]
				fn to_owned<'o, K: GenericHandleKind>(handle: &Self::Handle<'_, K>) -> Self::Handle<'o, Owned> {
					// same as above
					struct Hack<'b, T> { inner: &'b T }
					let $h = Hack { inner: &handle.inner };
					let $wf = $W::to_owned;
					$e
				}
			}
		};
	}

	wrapper!(impl[V: ValidateItem] (ValidatingTableHandleHKT => ValidatingTableHandle)[V] => TableHandleHKT
		|handle, wf| super::ValidatingTableHandle {
			inner: wf(handle.inner),
			_marker: PhantomData
		}
	);
	wrapper!(impl[V: ValidateItem] (ValidatingArrayHandleHKT => ValidatingArrayHandle)[V] => ArrayHandleHKT
		|handle, wf| super::ValidatingArrayHandle {
			inner: wf(handle.inner),
			_marker: PhantomData
		}
	);
}

/// A validating wrapper around an [`OccupiedTableEntry`]
/// that validates the inner value on access.
///
/// The inner value is still exposed, in case any exceptions need to be made.
#[allow(missing_docs)]
pub struct ValidatingOccupiedTableEntry<'a, V: ValidateItem> {
	pub inner: OccupiedTableEntry<'a>,
	_marker: PhantomData<V>,
}

/// A validating wrapper around a [`VacantTableEntry`]
/// that only allows inserting valid values.
///
/// The inner value is still exposed, in case any exceptions need to be made.
#[allow(missing_docs)]
pub struct ValidatingVacantTableEntry<'a, V: ValidateItem> {
	pub inner: VacantTableEntry<'a>,
	_marker: PhantomData<V>,
}

/// A validating version of [`TableEntry`].
#[allow(missing_docs)]
pub enum ValidatingTableEntry<'a, V: ValidateItem> {
	Occupied(ValidatingOccupiedTableEntry<'a, V>),
	Vacant(ValidatingVacantTableEntry<'a, V>),
}

impl<'a, V: ValidateItem> ValidatingOccupiedTableEntry<'a, V> {
	/// See [`OccupiedTableEntry::get_mut`].
	pub fn get_mut(&mut self) -> ValidateMutResult<ValidOutputMut<V>> {
		V::validate(self.inner.get_mut())
	}

	/// See [`OccupiedTableEntry::into_mut`].
	pub fn into_mut(self) -> ValidateMutResult<'a, ValidOutputMut<'a, V>> {
		V::validate(self.inner.into_mut())
	}
}

impl<'a, V: ValidateItem> ValidatingVacantTableEntry<'a, V> {
	/// See [`VacantTableEntry::insert`].
	pub fn insert(self, value: ValidOutputOwned<V>) -> ValidOutputMut<'a, V> {
		// note: `insert` doesn't change the value,
		// so this is just a roundtrip between `unvalidate` and `validate`.
		V::validate(self.inner.insert(V::unvalidate(value))).unwrap()
	}
}

impl<'a, V: ValidateItem> ValidatingTableEntry<'a, V> {
	/// See [`TableEntry::or_insert`].
	pub fn or_insert(
		self,
		default: ValidOutputOwned<V>,
	) -> ValidateMutResult<'a, ValidOutputMut<'a, V>> {
		match self {
			Self::Occupied(e) => e.into_mut(),
			Self::Vacant(e) => Ok(e.insert(default)),
		}
	}

	/// See [`TableEntry::or_insert_with`].
	pub fn or_insert_with<'o, F: FnOnce() -> ValidOutputOwned<'o, V>>(
		self,
		default: F,
	) -> ValidateMutResult<'a, ValidOutputMut<'a, V>> {
		match self {
			Self::Occupied(e) => e.into_mut(),
			Self::Vacant(e) => Ok(e.insert(default())),
		}
	}

	/// Convenience wrapper for `self.or_insert_with(Default::default)`.
	pub fn or_default<'o>(self) -> ValidateMutResult<'a, ValidOutputMut<'a, V>>
	where
		ValidOutputOwned<'o, V>: Default,
	{
		#[allow(
			clippy::unwrap_or_default
			/* reason: this is a false positive (it is not Option::or_insert_with) */
		)]
		self.or_insert_with(Default::default)
	}
}

impl<'a> TableMut<'a> {
	/// Validating wrapper around [`Self::get_mut`].
	pub fn get_mut_validate<V: ValidateItem>(
		self,
		key: &str,
	) -> PropResult<'a, ValidOutputMut<'a, V>> {
		self.get_mut(key).map(V::validate).transpose()
	}

	/// Validating wrapper around [`Self::entry`].
	pub fn validating_entry<V: ValidateItem>(self, key: &str) -> ValidatingTableEntry<'a, V> {
		match self.entry(key) {
			TableEntry::Occupied(inner) => {
				ValidatingTableEntry::Occupied(ValidatingOccupiedTableEntry {
					inner,
					_marker: PhantomData,
				})
			}
			TableEntry::Vacant(inner) => ValidatingTableEntry::Vacant(ValidatingVacantTableEntry {
				inner,
				_marker: PhantomData,
			}),
		}
	}
}

impl<'a> ArrayMut<'a> {
	/// Validating wrapper around [`Self::get_mut`].
	pub fn get_mut_validate<V: ValidateItem>(
		self,
		index: usize,
	) -> PropResult<'a, ValidOutputMut<'a, V>> {
		self.get_mut(index).map(V::validate).transpose()
	}
}

/// A validating wrapper around a [`TableHandle`]
/// that validates the entries on access.
///
/// The inner value is still exposed, in case any exceptions need to be made.
#[allow(missing_docs)]
#[perfect_derive(Debug, Clone)]
pub struct ValidatingTableHandle<'a, V: ValidateItem, K: GenericHandleKind = Dynamic> {
	pub inner: TableHandle<'a, K>,
	_marker: PhantomData<V>,
}

impl<V: ValidateItem, K: GenericHandleKind + FromOwned> Default
	for ValidatingTableHandle<'_, V, K>
{
	fn default() -> Self {
		Self::new(TableHandle::default())
	}
}

/// A validating wrapper around a [`ArrayHandle`]
/// that validates the elements on access.
///
/// The inner value is still exposed, in case any exceptions need to be made.
#[allow(missing_docs)]
#[perfect_derive(Debug, Clone)]
pub struct ValidatingArrayHandle<'a, V: ValidateItem, K: GenericHandleKind = Dynamic> {
	pub inner: ArrayHandle<'a, K>,
	_marker: PhantomData<V>,
}

impl<V: ValidateItem, K: GenericHandleKind + FromOwned> Default
	for ValidatingArrayHandle<'_, V, K>
{
	fn default() -> Self {
		Self::new(ArrayHandle::default())
	}
}

/// A validating wrapper around a [`DocumentMut`]
/// that validates the elements on access.
///
/// The inner value is still exposed, in case any exceptions need to be made.
#[allow(missing_docs)]
#[perfect_derive(Debug, Clone)]
pub struct ValidatingDocument<V: ValidateItem> {
	pub inner: DocumentMut,
	_marker: PhantomData<V>,
}

crate::shorthand_types! { [V]
	ValidatingTableHandle => ValidatingTableOwned, ValidatingTableMut,
	ValidatingArrayHandle => ValidatingArrayOwned, ValidatingArrayMut,
}

impl<'a, V: ValidateItem, K: GenericHandleKind> ValidatingTableHandle<'a, V, K> {
	/// Wrap the given table.
	#[inline]
	pub fn new(inner: TableHandle<'a, K>) -> Self {
		Self {
			inner,
			_marker: PhantomData,
		}
	}

	/// Wrapper around [`TableHandle::as_mut`].
	#[inline]
	pub fn as_mut(&mut self) -> ValidatingTableMut<V> {
		ValidatingTableHandleHKT::as_mut(self)
	}

	/// Wrapper around [`TableHandle::into_owned`].
	#[inline]
	pub fn into_owned<'o>(self) -> ValidatingTableOwned<'o, V> {
		ValidatingTableHandleHKT::into_owned(self)
	}

	/// Wrapper around [`TableHandle::to_owned`].
	#[inline]
	pub fn to_owned<'o>(&self) -> ValidatingTableOwned<'o, V> {
		ValidatingTableHandleHKT::to_owned(self)
	}
}

impl<'a, V: ValidateItem> ValidatingTableMut<'a, V> {
	/// Validating wrapper around [`TableHandle::get_mut`].
	#[inline]
	pub fn get_mut(self, key: &str) -> PropResult<'a, ValidOutputMut<'a, V>> {
		self.inner.get_mut_validate::<V>(key)
	}

	/// Validating wrapper around [`TableHandle::entry`].
	#[inline]
	pub fn entry(self, key: &str) -> ValidatingTableEntry<'a, V> {
		self.inner.validating_entry::<V>(key)
	}

	/// Wrapper around [`TableHandle::try_insert`].
	#[allow(clippy::result_large_err)]
	pub fn try_insert<'o>(
		self,
		key: &str,
		item: ValidOutputOwned<'o, V>,
	) -> Result<(), ItemOwned<'o>> {
		self.inner.try_insert(key, V::unvalidate(item))
	}

	/// Validating wrapper around [`TableHandle::iter_mut`].
	pub fn iter_mut(
		self,
	) -> impl Iterator<Item = (KeyMut<'a>, ValidateMutResult<'a, ValidOutputMut<'a, V>>)> {
		self.inner.iter_mut().map(|(k, v)| (k, V::validate(v)))
	}
}

impl<'a, V: ValidateItem, K: GenericHandleKind> ValidatingArrayHandle<'a, V, K> {
	/// Wrap the given array.
	#[inline]
	pub fn new(inner: ArrayHandle<'a, K>) -> Self {
		Self {
			inner,
			_marker: PhantomData,
		}
	}

	/// Wrapper around [`ArrayHandle::as_mut`].
	pub fn as_mut(&mut self) -> ValidatingArrayMut<V> {
		ValidatingArrayHandleHKT::as_mut(self)
	}

	/// Wrapper around [`ArrayHandle::into_owned`].
	#[inline]
	pub fn into_owned<'o>(self) -> ValidatingArrayOwned<'o, V> {
		ValidatingArrayHandleHKT::into_owned(self)
	}

	/// Wrapper around [`ArrayHandle::to_owned`].
	#[inline]
	pub fn to_owned<'o>(&self) -> ValidatingArrayOwned<'o, V> {
		ValidatingArrayHandleHKT::to_owned(self)
	}
}

impl<'a, V: ValidateItem> ValidatingArrayMut<'a, V> {
	/// Validating wrapper around [`ArrayHandle::get_mut`].
	pub fn get_mut(self, index: usize) -> PropResult<'a, ValidOutputMut<'a, V>> {
		self.inner.get_mut_validate::<V>(index)
	}

	/// Wrapper around [`ArrayHandle::try_push`].
	#[allow(clippy::result_large_err)]
	pub fn try_push(self, item: ValidOutputOwned<'_, V>) -> Result<(), ItemOwned<'_>> {
		self.inner.try_push(V::unvalidate(item))
	}

	/// Validating wrapper around [`ArrayHandle::iter_mut`].
	pub fn iter_mut(self) -> impl Iterator<Item = ValidateMutResult<'a, ValidOutputMut<'a, V>>> {
		self.inner.iter_mut().map(V::validate)
	}
}

impl<V: ValidateItem> ValidatingDocument<V> {
	/// Wrap the given document.
	pub fn new(inner: DocumentMut) -> Self {
		Self {
			inner,
			_marker: PhantomData,
		}
	}

	/// Validating wrapper around [`DocumentMut::as_table_mut`].
	pub fn as_table_mut(&mut self) -> ValidatingTableMut<V> {
		let root = self.inner.as_table_mut();
		ValidatingTableMut::new(TableHandle::Table(root))
	}
}
