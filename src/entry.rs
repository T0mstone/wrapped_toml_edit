//! Entry types that contain both inline and non-inline variants.

use crate::{ItemHandle, ItemMut, ItemOwned};

/// A view into an occupied entry in a table or inline table.
#[allow(missing_docs)]
pub enum OccupiedTableEntry<'a> {
	Table(toml_edit::OccupiedEntry<'a>),
	InlineTable(toml_edit::InlineOccupiedEntry<'a>),
}

/// A view into a vacant entry in a table or inline table.
#[allow(missing_docs)]
pub enum VacantTableEntry<'a> {
	Table(toml_edit::VacantEntry<'a>),
	InlineTable(toml_edit::InlineVacantEntry<'a>),
}

/// A view into a single location in a table or inline table,
/// which may be vacant or occupied.
#[allow(missing_docs)]
pub enum TableEntry<'a> {
	Occupied(OccupiedTableEntry<'a>),
	Vacant(VacantTableEntry<'a>),
}

impl<'a> OccupiedTableEntry<'a> {
	// TODO: see if https://github.com/toml-rs/toml/issues/683 results in this being justified
	const NONSENSICAL: &'static str = "OccupiedEntry was Item::None";

	/// Borrow the item in the entry mutably as a handle.
	pub fn get_mut(&mut self) -> ItemMut {
		match self {
			Self::Table(e) => ItemMut::new(e.get_mut()).expect(Self::NONSENSICAL),
			Self::InlineTable(e) => ItemMut::from_value(e.get_mut()),
		}
	}

	/// Like [`get_mut`](Self::get_mut),
	/// but takes `self` by value, allowing the longest possible output lifetime.
	pub fn into_mut(self) -> ItemMut<'a> {
		match self {
			Self::Table(e) => ItemMut::new(e.into_mut()).expect(Self::NONSENSICAL),
			Self::InlineTable(e) => ItemMut::from_value(e.into_mut()),
		}
	}
}

impl<'a> VacantTableEntry<'a> {
	/// Insert a value into the table at this entry's key.
	pub fn insert(self, item: ItemOwned) -> ItemMut<'a> {
		match self {
			Self::Table(e) => ItemHandle::new(e.insert(item.into_item())).unwrap_or_else(|| {
				unreachable!(
					"`VacantEntry::insert` returned `Item::None` for non-`Item::None` argument"
				)
			}),
			Self::InlineTable(e) => ItemHandle::from_value(e.insert(item.into_value())),
		}
	}
}

impl<'a> From<toml_edit::Entry<'a>> for TableEntry<'a> {
	fn from(entry: toml_edit::Entry<'a>) -> Self {
		match entry {
			toml_edit::Entry::Occupied(e) => Self::Occupied(OccupiedTableEntry::Table(e)),
			toml_edit::Entry::Vacant(e) => Self::Vacant(VacantTableEntry::Table(e)),
		}
	}
}

impl<'a> From<toml_edit::InlineEntry<'a>> for TableEntry<'a> {
	fn from(entry: toml_edit::InlineEntry<'a>) -> Self {
		match entry {
			toml_edit::InlineEntry::Occupied(e) => {
				Self::Occupied(OccupiedTableEntry::InlineTable(e))
			}
			toml_edit::InlineEntry::Vacant(e) => Self::Vacant(VacantTableEntry::InlineTable(e)),
		}
	}
}

impl<'a> TableEntry<'a> {
	/// Insert `default` into the entry if it is vacant
	/// and get a mutable handle to the item in the entry.
	pub fn or_insert(self, default: ItemOwned) -> ItemMut<'a> {
		match self {
			Self::Occupied(e) => e.into_mut(),
			Self::Vacant(e) => e.insert(default),
		}
	}

	/// Insert `default()` into the entry if it is vacant
	/// and get a mutable handle to the item in the entry.
	pub fn or_insert_with<'o, F: FnOnce() -> ItemOwned<'o>>(self, default: F) -> ItemMut<'a> {
		match self {
			Self::Occupied(e) => e.into_mut(),
			Self::Vacant(e) => e.insert(default()),
		}
	}
}
