This crate provides wrappers around `toml_edit`.

In particular, these fulfill two purposes:
- Be able to work with data of mixed ownership,
  for example a table you've borrowed from the original,
  but added a single owned entry to.
- Have more strongly-typed versions of the types,
  for example for a configuration file.
  For this, see [`validate`].

Crate features:
- all features from `toml_edit` are re-exported under the same names
- `either`: enable compatibility impls with the `either` crate.